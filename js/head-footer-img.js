var screen_width = document.body.clientWidth; 
var search_type=1; // 1-搜索产品,2-搜索新闻
if (window.navigator.userAgent.indexOf("MSIE") >= 1||"ActiveXObject" in window) {
	$("body").addClass("ie-class");
}
function handleShowSearch() { // 打开搜索框
	screen_width = document.body.clientWidth; 
	chooseSearchType(1); //每次打开搜索框，默认是搜索“产品”
	$('#search-nav').show()
	$('.section-nav').addClass('show')
	// getSearchData();
	$('#mobile-search').hide()
	$('#mobile-hamburg').hide()
	$('#mobile-cha').show()
	$('#mask').show()
	if(screen_width<769){
		$('.search-nav .search-nav-warp input').animate({left: (screen_width/100)+'px'}, 300)
	}else{
		$('.search-nav .search-nav-warp input').animate({left: '0'}, 300)
	}
	$('.nav-container-title').css({ 'border-bottom': '0px solid #eee' })
	if(screen_width>768&&screen_width<1024){
		$(".section-nav .head-footer .nav-body #mobile-nav").removeClass("visible-1024");
	}
}
function handleHideSearch() { // 关闭搜索框
	if(!$(".section-nav .head-footer .nav-body #mobile-nav").hasClass("visible-1024")){
		$(".section-nav .head-footer .nav-body #mobile-nav").addClass("visible-1024");
	}
	$('#search-nav').hide()
	$('#mask').hide()
	$('.search-nav .search-nav-warp input').css({ 'left': '100%' })
	$('.nav-container-title').css({ 'border-bottom': '1px solid #eee' })
}
function parseListUrl(bname, cname) {
	window.location.href = 'https://product.sanyglobal.com/' + bname + '/' + cname + '/'
}
function hamburg() { //手机端的汉堡图标
	$('#mobile-link-content').show()
	$('.section-nav').addClass('show')
	$('#mobile-search').hide()
	$('#mobile-hamburg').hide()
	$('#mobile-cha').show()
	$('#mask').show()
	$('.nav-container-title').css({ 'border-bottom': '1px solid #eee' })
}
function closeCurrent() { // 手机端的叉关闭图标
	if(!$(".section-nav .head-footer .nav-body #mobile-nav").hasClass("visible-1024")){
		$(".section-nav .head-footer .nav-body #mobile-nav").addClass("visible-1024");
	}
	$('#mobile-link-content').hide()
	$('.section-nav').removeClass('show')
	$('#mobile-search').show()
	$('#mobile-hamburg').show()
	$('#mobile-cha').hide()
	$('#search-nav').hide()
	$('#mask').hide()
	$('.search-nav .search-nav-warp input').css({ 'left': '100%' })
}
function chooseSearchType(index){
	search_type=index
	$('input[name=type]').val([search_type]);
	$(".search-recommond-buttons div").removeClass('active');
	$(".search-recommond-buttons div").eq(index-1).addClass('active');
}
function submitData(data) { // Quick Links搜索
	window.location.href = 'https://product.sanyglobal.com/search?key=' + data+'&type='+search_type;
}
function getLinks(){
	$.ajax({
	    url: productHttpUrl+"/v1/product/get-banners",
	    data:{"group_id":3},
	    type: 'GET',
	    success: function(res) {
	        if(res.code==0){
	        	if(res.data.length>0){
	        		$('.search-nav .search-recommond-title').show();
	        		var html = '';
					var arr = res.data;
					for(var i=0;i<arr.length;i++){
						html += '<li><a href="'+arr[i].ad_link+'">'+arr[i].ad_name+'</a></li>'
						$('.search-recommond-links ul').html(html);
					}
	        	}
	        }
	    }
    });
}

$(function () {
	//获取Quick Links
	getLinks();
	// 手机端菜单伸缩折叠
	$('.mobile-link-item').click(function () { 
		var flag = false
		if ($(this).find('.mobile-child-link').is(':visible')) {
			flag = true
		}
		$('.mobile-link-item').find('.mobile-child-link').hide()
		$('.mobile-link-item').find('.nav-close img').removeClass('openitemImg')
		$(this).find('.mobile-child-link').toggle()
		if (flag) {
			$(this).find('.mobile-child-link').toggle()
			$(this).find('.nav-close img').removeClass('openitemImg')
		} else {
			$(this).find('.nav-close img').addClass('openitemImg')
		}
	})

	$('.nav .nav-common-bkg').css('display', 'none')
	$('.mouse-hover-item').mouseenter(function () {
		$('.nav-body .mask').show()
		var height = $(this).find('.nav-common').outerHeight() + 'px'
		$('.nav .nav-common-bkg').css({ 'display': 'block', 'height': height })
	})
	$('.mouse-hover-item').mouseleave(function () {
		$('.nav .nav-common-bkg').css('display', 'none')
		$('.nav-body .mask').hide()
	})

	$('.nav .nav-main .mouse-hover-shade').mouseenter(function () {
		$('.nav-body .mask').show()
	})
	$('.nav .nav-main .mouse-hover-shade').mouseleave(function () {
		$('.nav-body .mask').hide()
	})
	//多语言覆盖
	$('.nav .other-nav .relative').mouseenter(function () {
		$('.nav-body .mask').show()
	})
	$('.nav .other-nav .relative').mouseleave(function () {
		$('.nav-body .mask').hide()
	})

	$('#input-warp').keydown(function (event) { //搜索按钮回车按键
		if (event.keyCode == 13) {
			window.location.href = 'https://product.sanyglobal.com/search?key=' + $('#search_value').val()+'&type='+search_type
		}
	})
	var scrollTopRec = 0, directionRec = 0
	$('.nav-body .nav').addClass('fixed')
	window.addEventListener('scroll', function () {
		var scrollTop = window.pageYOffset || document.documentElement.scrollTop ||
			document.body.scrollTop
		if (document.body.clientWidth < 1024 && scrollTop < 5) {
			return
		}
		var distance = scrollTop - scrollTopRec
		if (Math.abs(distance) >= 60) {
			scrollTopRec = scrollTop
			if (distance <= -60 && directionRec == 1) {
				directionRec = 0
				$('.nav-body .nav').css({ 'transform': 'translate(0px, 0px)', 'transition': 'all 0.3s ease 0s' })
			} else if (distance >= 60 && directionRec == 0 && scrollTopRec > 50) {
				directionRec = 1
				$('.nav-body .nav').css({ 'transform': 'translate(0px, -100%)', 'transition': 'all 0s ease 0s' })
			}
		}
	})
	
	//获取当前的年份
	var myDate = new Date();
	var year = myDate.getFullYear();
	$(".myyear").html(year);
})

