/*	
 	messageCookie.js
	Enquiry message source of landing page and page setup
	version 0.2
	create by zhengyc 
	create time 20180629
	
	use:  
	   1:form input 
	    <input type="hidden" id="landingSite" name="landingSite" value=''>
 		<input type="hidden" name="laiyuan" value="" id="laiyuan"> 

 	   2:script
 	    $(function(){
			cookieObject.init();
			cookieObject.setInput('laiyuan','landingSite');
		});
	note:
	   After the initialization of "input" assignment  	
*/

cookieObject = {
	getCookie: function(name){
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i=0; i < ca.length; i++)
		{
		   var c = ca[i].trim();
		   if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	 },
	// 设置cookie
	setCookie: function(name, value, days, domain){
		var domain, domainParts, date, expires, host;
		if (days){
		   date = new Date();
		   date.setTime(date.getTime()+(days*24*60*60*1000));
		   expires = "; expires="+date.toGMTString();
		}else{
		   expires = "";
		}
		host = location.host;
		if (host.split('.').length === 1){
		   document.cookie = name+"="+value+expires+"; path=/";
		}else{
		   domainParts = host.split('.');
		   domainParts.shift();
		   domain = domain?domain:'.'+domainParts.join('.');
		   document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
		   if (this.getCookie(name) == null || this.getCookie(name) != value)
		   {
			  domain = '.'+host;
			  document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
		   }
		}
	 },
	 delCookie: function(name){
		this.setCookie(name, '', -1);
	 },
	setInput:function(inputId_A,inputId_B) {
		var v1=sessionStorage.getItem('source_url')?sessionStorage.getItem('source_url'):"",v2=sessionStorage.getItem('firstpage')?sessionStorage.getItem('firstpage'):"";
		$("#"+inputId_A ).val(v1);
	  $("#"+inputId_B).val(v2);
	},
	setInputNew:function(inputId_A,inputId_B) {
		var v3=sessionStorage.getItem(inputId_B)?sessionStorage.getItem(inputId_B):"";
		$("#"+inputId_A ).val(v3);
	},
	init:function(){
		var currenturl=window.location.href;
		var currenturl2=currenturl.split("?")[0];
		var currenthost = window.location.host;
		var referurl = document.referrer;
		var link_a = document.createElement('a');
		var pageTitle=document.title.substr(0,40);
		var days=1;
		var startTime=sessionStorage.getItem('startTime')||this.getCookie("startTime");
		var staytime=sessionStorage.getItem('staytime')||this.getCookie("staytime");
		var urlhistory=sessionStorage.getItem('urlhistory')||this.getCookie("urlhistory");
		if(!urlhistory){
			if(currenturl){
				sessionStorage.setItem('urlhistory',currenturl2);
				sessionStorage.setItem('startTime',(new Date()).getTime()/1000);
				sessionStorage.setItem('staytime',0);
				this.setCookie('urlhistory',currenturl2,days);
				this.setCookie('startTime',(new Date()).getTime()/1000,days);
				this.setCookie('staytime',0,days);
			}
		}else{
			var arr=urlhistory.split(">>"),newTime=(new Date()).getTime()/1000;
			if(currenturl2&&currenturl2!=arr[arr.length-1].split("(")[0]){
				sessionStorage.setItem('staytime',((new Date()).getTime()/1000-startTime).toFixed(2));
				sessionStorage.setItem('urlhistory',urlhistory+'('+(newTime-startTime-staytime).toFixed(2)+'s)'+'>>'+currenturl2);
				this.setCookie('urlhistory',urlhistory+'('+(newTime-startTime-staytime).toFixed(2)+'s)'+'>>'+currenturl2,days);
				this.setCookie('staytime',((new Date()).getTime()/1000-startTime).toFixed(2),days);
			}
		}
		var source_url = sessionStorage.getItem('source_url');
		var firstpage = sessionStorage.getItem('firstpage');
		if(!source_url){
			source_url=this.getCookie("source_url")||referurl||currenturl;
			sessionStorage.setItem('source_url',source_url);
			this.setCookie('source_url',source_url,days);
		}
		if(!firstpage){
			firstpage=this.getCookie("firstpage")||currenturl;
			sessionStorage.setItem('firstpage',firstpage);
			this.setCookie('firstpage',firstpage,days);
		}
	}
}