(window.webpackJsonp = window.webpackJsonp || []).push([[1], {
    563: function (t, e, c) {
        "use strict";
        var l = {
            props: ["src", "ratio"], data: function () {
                return {curRatio: 1, loaded: !1, fit: "cover"}
            }, mounted: function () {
            }, methods: {
                handleImage: function (t) {
                    var e = t.currentTarget.width, c = t.currentTarget.height;
                    this.curRatio = e / c, this.loaded = !0
                }
            }
        }, r = (c(572), c(11)), component = Object(r.a)(l, (function () {
            var t = this.$createElement, e = this._self._c || t;
            return e("div", {staticStyle: {background: "none"}}, [this.src ? e("img", {
                staticClass: "handel-image",
                class: this.curRatio > this.ratio ? "wdt" : "hgt",
                attrs: {src: this.src},
                on: {load: this.handleImage}
            }) : e("div", {staticClass: "no-image"})])
        }), [], !1, null, null, null);
        e.a = component.exports
    }, 564: function (t, e, c) {
    }, 565: function (t, e, c) {
    }, 566: function (t, e, c) {
        "use strict";
        c.r(e);
        var l = {
            props: {value: {}, type: ""}, data: function () {
                return {}
            }, created: function () {
            }, methods: {}
        }, r = (c(575), c(11)), component = Object(r.a)(l, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {staticClass: "breadcrumb-top"}, [c("div", {staticClass: "breadcrumb-cl moudle container"}, [c("div", {staticClass: "breadcrumbs"}, t._l(t.value, (function (e, l) {
                return c("span", {key: l}, [e.url && 2 == e.type ? c("a", {
                    class: l === t.value.length - 1 && 1 === t.type ? "current" : "",
                    attrs: {href: e.url}
                }, [t._v(t._s(e.name))]) : t._e(), t._v(" "), e.url && 2 != e.type ? c("a", {
                    class: l === t.value.length - 1 && 1 === t.type ? "current" : "",
                    attrs: {href: e.url}
                }, [t._v(t._s(e.name))]) : c("a", {attrs: {href: "javascript:;"}}), t._v(" "), l < t.value.length - 1 ? c("i", {staticClass: "el-breadcrumb__separator el-icon-arrow-right"}) : t._e()])
            })), 0)])])
        }), [], !1, null, null, null);
        e.default = component.exports
    }, 569: function (t, e, c) {
        "use strict";
        c.r(e);
        c(195);
        var l = c(15), r = c(563), n = {
            components: {myImage: r.a}, props: ["imageList", "imageIndex"], data: function () {
                return {list: [], previewFlag: !1, shortBase: "?x-oss-process=style/goods_gallary"}
            }, created: function () {
                this.list = this.imageList, this.index = this.imageIndex
            }, methods: {
                previewImgFunction: function (t) {
                    this.$emit("previewImgFun", t)
                }
            }
        }, o = (c(634), c(11)), v = Object(o.a)(n, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {staticClass: "imageList"}, [6 == t.list.length ? c("div", {staticClass: "image6"}, [c("div", {staticClass: "gallery-img-left"}, [t._l(3, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e - 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {loadmethod: 1, src: t.list[e - 1] + t.shortBase + "6_" + (1 === e ? 1 : 2), ratio: "1.766"}
                })], 1)])]
            }))], 2), t._v(" "), c("div", {staticClass: "gallery-img-right"}, [t._l(3, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e + 2)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {loadmethod: 1, src: t.list[e + 2] + t.shortBase + "6_" + (3 === e ? 1 : 2), ratio: "1.778"}
                })], 1)])]
            }))], 2)]) : 5 == t.list.length ? c("div", {staticClass: "image5"}, [c("div", {staticClass: "gallery-img-left"}, [t._l(3, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e - 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {loadmethod: 1, src: t.list[e - 1] + t.shortBase + "5_" + e, ratio: "1.766"}
                })], 1)])]
            }))], 2), t._v(" "), c("div", {staticClass: "gallery-img-right"}, [t._l(2, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e + 2)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {loadmethod: 1, src: t.list[e + 2] + t.shortBase + "5_1", ratio: 1 == e ? "3.532" : "1.766"}
                })], 1)])]
            }))], 2)]) : 4 == t.list.length ? c("div", {staticClass: "image4"}, [c("div", {staticClass: "gallery-img-left"}, [t._l(2, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e - 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {
                        loadmethod: 1,
                        src: t.list[e - 1] + t.shortBase + "4_" + e,
                        ratio: 1 == e ? "1.766" : "3.532"
                    }
                })], 1)])]
            }))], 2), t._v(" "), c("div", {staticClass: "gallery-img-right"}, [t._l(2, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e + 1] && t.previewImgFunction(6 * t.index + e + 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {
                        loadmethod: 1,
                        src: t.list[e + 1] + t.shortBase + "4_" + (2 === e ? 1 : 2),
                        ratio: 2 == e ? "1.766" : "3.532"
                    }
                })], 1)])]
            }))], 2)]) : 3 == t.list.length ? c("div", {staticClass: "image3"}, [c("div", {staticClass: "gallery-img-left"}, [t._l(1, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e - 1] && t.previewImgFunction(6 * t.index + e - 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {loadmethod: 1, src: t.list[e - 1] + t.shortBase + "3_" + e, ratio: "1.167"}
                })], 1)])]
            }))], 2), t._v(" "), c("div", {staticClass: "gallery-img-right"}, [t._l(2, (function (e) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            t.list[e] && t.previewImgFunction(6 * t.index + e)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + (e + 1),
                    attrs: {
                        loadmethod: 1,
                        src: t.list[e] + t.shortBase + "3_" + (e + 1),
                        ratio: 1 == e ? "3.532" : "1.766"
                    }
                })], 1)])]
            }))], 2)]) : 2 == t.list.length ? c("div", {staticClass: "image2"}, [t._l(2, (function (e) {
                return [c("div", {
                    key: e,
                    class: 1 === e ? "gallery-img-left" : "gallery-img-right"
                }, [c("div", {
                    staticClass: "image-item", on: {
                        click: function (c) {
                            return t.previewImgFunction(6 * t.index + e - 1)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    attrs: {
                        loadmethod: 1,
                        src: t.list[e - 1] + t.shortBase + "2_1",
                        ratio: "1.167"
                    }
                })], 1)])])]
            }))], 2) : 1 == t.list.length ? c("div", {staticClass: "image1"}, [c("div", {
                staticClass: "image-item",
                on: {
                    click: function (e) {
                        t.list[0] && t.previewImgFunction(6 * t.index)
                    }
                }
            }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                attrs: {
                    loadmethod: 1,
                    src: t.list[0] + t.shortBase + "1_1",
                    ratio: "2.32"
                }
            })], 1)])]) : t._e()])
        }), [], !1, null, null, null).exports, d = {
            components: {myImage: r.a}, props: {imageList: {}, imageIndex: {}}, data: function () {
                return {list: [], previewFlag: !1, shortBase: "?x-oss-process=style/goods_gallary_m"}
            }, created: function () {
                this.list = this.imageList, this.index = this.imageIndex
            }, methods: {
                previewImgFunction: function (t) {
                    this.$emit("previewImgFun", t)
                }
            }
        }, m = (c(635), Object(o.a)(d, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {staticClass: "imageListPhone"}, [3 == t.list.length ? c("div", {staticClass: "image3"}, [c("div", {staticClass: "gallery-img-mobile"}, [t._l(t.list, (function (e, l) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            e && t.previewImgFunction(3 * t.index + l - 3)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {src: e + t.shortBase + "3_" + (3 === e ? 1 : 2), ratio: "1.757"}
                })], 1)])]
            }))], 2)]) : 2 == t.list.length ? c("div", {staticClass: "image2"}, [c("div", {staticClass: "gallery-img-mobile"}, [t._l(t.list, (function (e, l) {
                return [c("div", {
                    key: "img_" + e, staticClass: "image-item", on: {
                        click: function (c) {
                            e && t.previewImgFunction(3 * t.index + l)
                        }
                    }
                }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                    key: "img_" + e,
                    attrs: {src: e + t.shortBase + "2_1", ratio: "0.572"}
                })], 1)])]
            }))], 2)]) : 1 == t.list.length ? c("div", {staticClass: "image1"}, [c("div", {staticClass: "gallery-img-mobile"}, [c("div", {
                staticClass: "image-item",
                on: {
                    click: function (e) {
                        t.list[0] && t.previewImgFunction(3 * t.index)
                    }
                }
            }, [c("div", {staticClass: "img-warp"}, [c("my-image", {
                attrs: {
                    src: t.list[0] + t.shortBase + "1_1",
                    ratio: "1.8"
                }
            })], 1)])])]) : t._e()])
        }), [], !1, null, null, null).exports), h = {
            notNextTick: !0,
            loop: !1,
            effect: "slide",
            initialSlide: 0,
            autoplay: !1,
            speed: 800,
            direction: "horizontal",
            grabCursor: !0,
            on: {
                slideChangeTransitionEnd: function () {
                }
            },
            lazy: {loadPrevNext: !0},
            pagination: {el: ".ga-swiper-pagination", clickable: !0}
        }, f = JSON.parse(JSON.stringify(h));
        f.pagination.el = ".image-swiper-pagination", f.navigation = {
            nextEl: ".image-button-next",
            prevEl: ".image-button-prev"
        };
        var _ = {
            components: {myImage: r.a, imageList: v, imageListPhone: m},
            props: {value: {}, title: ""},
            data: function () {
                return {
                    src: "",
                    ratio: "",
                    vrData: {},
                    images: [],
                    images1: [],
                    prevImages: [],
                    prevImages1: [],
                    videoUrl: "",
                    galleryActiveIndex: 0,
                    previewFlag: !1,
                    previewVr: !1,
                    fits: ["cover"],
                    loaded: !1,
                    fit: "fill",
                    list: [],
                    imgIndex: "",
                    swiperOption: h,
                    swiperConfigImages: f,
                    video: {
                        url: "",
                        cover: "",
                        muted: !0,
                        loop: !1,
                        preload: "auto",
                        poster: "",
                        volume: 1,
                        autoplay: !1
                    },
                    elHeight: 0,
                    equipment: "",
                    pcGallery1: "?x-oss-process=style/goods_gallary1",
                    pcGallery2: "?x-oss-process=style/goods_gallary2",
                    pcVr: "?x-oss-process=style/goods_vr",
                    phoneGallery1: "?x-oss-process=style/goods_gallary1_m",
                    phoneGallery2: "?x-oss-process=style/goods_gallary2_m",
                    phoneVr: "?x-oss-process=style/goods_vr_m",
                    isPlaying: !1
                }
            },
            created: function () {
            },
            mounted: function () {
                var t = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                this.ratio = t > 1024 ? 2.32 : 1.778, this.isMobile()
            },
            computed: {
                $video: function () {
                    if (this.$refs.galleryVideo) return this.$refs.galleryVideo
                }
            },
            methods: {
                isMobile: function () {
                    var t = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
                    this.equipment = t ? "phone" : "pc", this.done()
                }, handleFullscreen: function () {
                }, previewImgFun: function (t) {
                    Object(l.i)(), this.previewFlag = !0, this.imageSwiper.slideTo(t)
                }, closePreview: function () {
                    Object(l.l)(), this.previewFlag = !1, this.previewVr = !1
                }, done: function () {
                    var t = this.value.vrValue;
                    (t = "string" == typeof t ? JSON.parse(t) : t) && t.vr_img && ("pc" == this.equipment ? t.vr_img = this.base.imgUrl + t.vr_img + this.pcVr : t.vr_img = this.base.imgUrl + t.vr_img + this.phoneVr), this.vrData = t;
                    var e = this.value.otherValue;
                    if (e && e.medias && e.medias.videos[0] && (this.videoUrl = this.base.imgUrl + e.medias.videos[0], this.video.url = this.videoUrl), e && e.medias && e.medias.images) {
                        this.allImages = e.medias.images;
                        for (var c = [], l = [], r = e.medias.images.slice(), n = r.length, o = Math.ceil(n / 6), v = Math.ceil(n / 3), d = [], m = [], i = 0; i < o; i++) {
                            for (var h = [], f = 6 * i; f < Math.min(n, 6 * (i + 1)); f++) h.push(this.base.imgUrl + r[f]), l.push(this.base.imgUrl + r[f]);
                            c.push(h.slice())
                        }
                        for (var _ = 0; _ < v; _++) {
                            for (var y = [], C = 3 * _; C < Math.min(n, 3 * (_ + 1)); C++) y.push(this.base.imgUrl + r[C]), m.push(this.base.imgUrl + r[C]);
                            d.push(y.slice())
                        }
                        this.images = c.slice(), this.images1 = d.slice(), this.prevImages = l.slice(), this.prevImages1 = m.slice()
                    }
                    this.loaded = !0
                }, toVr: function () {
                    this.vrData.vr_url ? (Object(l.i)(), this.previewVr = !0) : window.open("https://www.sanyglobal.com/feature/vr-view/index.html")
                }, closePreVr: function () {
                    Object(l.l)(), this.previewVr = !1
                }, galleryClick: function (t) {
                    this.galleryActiveIndex !== t && (this.galleryActiveIndex = t, this.$video && (1 !== t || this.isPlaying ? 1 !== t && this.isPlaying && (this.$video.pause(), this.isPlaying = !1) : (this.$video.play(), this.isPlaying = !0)))
                }, videoPlay: function () {
                    this.isPlaying ? (this.$video.pause(), this.isPlaying = !1) : (this.$video.play(), this.isPlaying = !0)
                }, videoClick: function () {
                    this.isPlaying = this.$video.paused
                }, getVideoStatus: function () {
                    this.isPlaying = !this.$video.paused
                }
            }
        }, y = (c(636), Object(o.a)(_, (function () {
            var t = this, e = t.$createElement, l = t._self._c || e;
            return l("div", {
                staticClass: "section-gallery",
                style: {position: "relative", "z-index": t.previewFlag || t.previewVr ? 999999 : 2},
                attrs: {id: "gallery"}
            }, [l("div", {
                ref: "gallery",
                staticClass: "moudle container"
            }, [l("h2", {staticClass: "moudle-title font-2"}, [t._v("Gallery")]), t._v(" "), l("div", {}, [l("div", {staticClass: "detail-maodian"}, [l("div", {staticClass: "visible-xs"}, [l("div", {staticClass: "detail-maodian-value gallery-detail-maodian-value"}, [l("div", {
                staticClass: "pc-gallery-item",
                class: "0" == t.galleryActiveIndex ? "active" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(0)
                    }
                }
            }, [t._v("Images")]), t._v(" "), l("div", {
                staticClass: "pc-gallery-item",
                class: "1" == t.galleryActiveIndex ? "active" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(1)
                    }
                }
            }, [t._v("Video")]), t._v(" "), l("div", {
                staticClass: "pc-gallery-item",
                class: "2" == t.galleryActiveIndex ? "active" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(2)
                    }
                }
            }, [t._v("VR View")])])]), t._v(" "), l("div", {staticClass: "hidden-xs mt-div gallery-mt-div"}, [l("div", {staticClass: "pc-maodian-s font-6 gallery-pc-maodian-s"}, [l("div", {
                staticClass: "pc-gallery-item",
                class: "0" == t.galleryActiveIndex ? "activepc" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(0)
                    }
                }
            }, [t._v("Images")]), t._v(" "), l("div", {staticClass: "pc-maodian-s-line"}), t._v(" "), l("div", {
                staticClass: "pc-gallery-item",
                class: "1" == t.galleryActiveIndex ? "activepc" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(1)
                    }
                }
            }, [t._v("Video")]), t._v(" "), l("div", {staticClass: "pc-maodian-s-line"}), t._v(" "), l("div", {
                staticClass: "pc-gallery-item",
                class: "2" == t.galleryActiveIndex ? "activepc" : "",
                on: {
                    click: function (e) {
                        return t.galleryClick(2)
                    }
                }
            }, [t._v("VR View")])])])]), t._v(" "), l("div", {staticClass: "gallery-moudle-content"}, [l("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: "0" == t.galleryActiveIndex,
                    expression: "galleryActiveIndex=='0'"
                }]
            }, [t.loaded ? l("div", {
                staticClass: "block",
                attrs: {id: "block"}
            }, [l("el-carousel", {
                ref: "photoCarousel",
                staticClass: "hidden-1024",
                attrs: {autoplay: !1, interval: 9e5, arrow: t.images.length > 1 ? "hover" : "never"}
            }, t._l(t.images, (function (e, c) {
                return l("el-carousel-item", {key: "list_" + c}, [l("imageList", {
                    attrs: {imageList: e, imageIndex: c},
                    on: {previewImgFun: t.previewImgFun}
                })], 1)
            })), 1), t._v(" "), l("div", {staticClass: "visible-1024 gallery-img-warp-m"}, [t.loaded ? l("div", {
                directives: [{
                    name: "swiper",
                    rawName: "v-swiper:mySwiper",
                    value: t.swiperOption,
                    expression: "swiperOption",
                    arg: "mySwiper"
                }]
            }, [l("div", {staticClass: "swiper-wrapper"}, t._l(t.images1.length, (function (e) {
                return l("div", {
                    key: "list" + e,
                    staticClass: "swiper-slide"
                }, [l("imageListPhone", {
                    attrs: {imageList: t.images1[e - 1], imageIndex: e},
                    on: {previewImgFun: t.previewImgFun}
                })], 1)
            })), 0)]) : t._e(), t._v(" "), l("div", {staticClass: "ga-swiper-pagination swiper-pagination swiper-pagination-bullets"})])], 1) : t._e()]), t._v(" "), l("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: "1" == t.galleryActiveIndex,
                    expression: "galleryActiveIndex=='1'"
                }]
            }, [l("div", {staticClass: "video-content"}, [t.video.url ? l("div", {staticClass: "video-warp"}, [l("video", {
                ref: "galleryVideo",
                staticClass: "video-item",
                attrs: {src: t.video.url, muted: "true", controls: "controls"},
                domProps: {muted: !0},
                on: {click: t.videoClick, timeupdate: t.getVideoStatus}
            }), t._v(" "), l("span", {
                staticClass: "play-btn",
                on: {click: t.videoPlay}
            }, [l("img", {
                class: t.isPlaying ? "playing" : "",
                attrs: {src: t.isPlaying ? "/icon/pause.svg" : "/icon/play.svg"}
            })])]) : t._e()])]), t._v(" "), l("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: "2" == t.galleryActiveIndex,
                    expression: "galleryActiveIndex=='2'"
                }]
            }, [l("div", {
                staticClass: "vr-content",
                on: {click: t.toVr}
            }, [t.vrData && t.vrData.vr_img ? [l("div", {staticClass: "vr-warp"}, [l("my-image", {
                attrs: {
                    src: t.vrData.vr_img,
                    ratio: t.ratio
                }
            })], 1), t._v(" "), t.vrData.vr_img ? l("img", {
                staticClass: "vrSvg",
                attrs: {src: c(633)}
            }) : t._e()] : t._e()], 2)])])])]), t._v(" "), l("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.previewFlag,
                    expression: "previewFlag"
                }], staticClass: "outer-tc images-poup full-screen"
            }, [l("div", {staticClass: "preview-pic"}, [l("div", {
                directives: [{
                    name: "swiper",
                    rawName: "v-swiper:imageSwiper",
                    value: t.swiperConfigImages,
                    expression: "swiperConfigImages",
                    arg: "imageSwiper"
                }]
            }, [l("div", {
                staticClass: "swiper-wrapper",
                staticStyle: {height: "100vh", width: "100vw"}
            }, t._l(t.prevImages, (function (e) {
                return l("div", {
                    key: e,
                    staticClass: "swiper-slide"
                }, [e ? l("el-image", {
                    staticStyle: {height: "100vh", width: "100vw"},
                    attrs: {src: e.split("?")[0], fit: "contain"}
                }) : t._e()], 1)
            })), 0)]), t._v(" "), t._m(0), t._v(" "), t._m(1), t._v(" "), l("div", {staticClass: "image-swiper-pagination swiper-pagination swiper-pagination-bullets"})]), t._v(" "), l("div", {staticClass: "poup-title"}, [l("div", {staticClass: "poup-container"}, [t._v(t._s(t.title)), l("span", {
                staticClass: "el-icon-close poup-close",
                on: {click: t.closePreview}
            })])])]), t._v(" "), t.previewVr ? l("div", {staticClass: "vr-poup"}, [l("div", {staticClass: "vr-poup-content"}, [l("iframe", {
                attrs: {
                    src: t.vrData.vr_url,
                    frameborder: "0",
                    seamless: ""
                }
            })]), t._v(" "), l("div", {staticClass: "poup-title"}, [l("div", {staticClass: "poup-container"}, [t._v(t._s(t.title)), l("span", {
                staticClass: "el-icon-close poup-close",
                on: {click: t.closePreview}
            })])])]) : t._e()])
        }), [function () {
            var t = this.$createElement, e = this._self._c || t;
            return e("button", {
                staticClass: "el-carousel__arrow el-carousel__arrow--right image-button-next",
                attrs: {type: "button"}
            }, [e("i", {staticClass: "el-icon-arrow-right"})])
        }, function () {
            var t = this.$createElement, e = this._self._c || t;
            return e("button", {
                staticClass: "el-carousel__arrow el-carousel__arrow--left image-button-prev",
                attrs: {type: "button"}
            }, [e("i", {staticClass: "el-icon-arrow-left"})])
        }], !1, null, null, null));
        e.default = y.exports
    }, 570: function (t, e, c) {
        "use strict";
        c.r(e);
        c(140);
        var l = c(53), r = {
            props: {value: {}}, data: function () {
                return {activeNames: [0], features: []}
            }, created: function () {
                var t = this;
                Object(l.a)(this.value).map((function (e) {
                    e.feat_desc_arr = e.feat_desc.split("\n"), t.features.push(e)
                }))
            }, methods: {
                handleChange: function (t) {
                }
            }
        }, n = (c(631), c(11)), component = Object(n.a)(r, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {attrs: {id: "features"}}, [0 != t.features.length ? c("div", {
                ref: "features",
                staticClass: "moudle container"
            }, [c("h2", {staticClass: "moudle-title font-2"}, [t._v("Features")]), t._v(" "), c("div", {staticClass: "moudle-value"}, [c("div", {staticClass: "moudle-value-el"}, [c("el-collapse", {
                on: {change: t.handleChange},
                model: {
                    value: t.activeNames, callback: function (e) {
                        t.activeNames = e
                    }, expression: "activeNames"
                }
            }, t._l(t.features, (function (e, l) {
                return c("el-collapse-item", {
                    key: l,
                    attrs: {title: e.feat_name, name: l}
                }, [c("div", {staticClass: "value-el-desc font-9"}, t._l(e.feat_desc_arr, (function (e, l) {
                    return c("p", {key: l}, [t._v("\n\t\t\t\t\t\t\t\t" + t._s(e) + "\n\t\t\t\t\t\t\t")])
                })), 0), t._v(" "), c("div", {staticClass: "value-el-img"}, [e.feat_images ? c("img", {attrs: {src: e.feat_images}}) : t._e()])])
            })), 1)], 1)])]) : t._e()])
        }), [], !1, null, null, null);
        e.default = component.exports
    }, 571: function (t, e, c) {
        "use strict";
        c.r(e);
        var l = {
            props: {type: {}}, data: function () {
                return {activeIndex: 0, barFixed: !1}
            }, created: function () {
            }, mounted: function () {
                window.addEventListener("scroll", this.handleScroll, !0)
            }, destroyed: function () {
                window.removeEventListener("scroll", this.handleScroll, !0)
            }, methods: {
                navchick: function (t, e) {
                    "" !== t && this.activeIndex !== t && (this.activeIndex = t);
                    var c = document.getElementById(e).offsetTop - 50;
                    window.scrollTo({top: c, behavior: "smooth"})
                }, handleScroll: function () {
                    var t = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
                        e = t > document.querySelector("#searchBar").offsetTop - 0;
                    this.barFixed = e > 0;
                    var c = document.querySelector("#features").offsetTop,
                        l = document.querySelector("#specs").offsetTop,
                        r = document.querySelector("#gallery").offsetTop;
                    t + 70 >= document.querySelector("#case").offsetTop ? this.activeIndex = 3 : t + 70 >= r ? this.activeIndex = 2 : t + 70 >= l ? this.activeIndex = 1 : t + 70 >= c && (this.activeIndex = 0)
                }, brochure: function () {
                    window.open(this.type.brochureUrl)
                }
            }
        }, r = (c(637), c(11)), component = Object(r.a)(l, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {
                staticClass: "detail-warp",
                attrs: {id: "searchBar"}
            }, [c("div", {staticClass: "detail-maodian"}, [c("div", {staticClass: "visible-xs"}, [c("div", {staticClass: "detail-maodian-value"}, [0 != t.type.feature ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(0, "features")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "0" == t.activeIndex ? "active" : ""
            }, [t._v("Features")])]) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(1, "specs")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "1" == t.activeIndex ? "active" : ""
            }, [t._v("Specs & Compare")])]) : t._e(), t._v(" "), c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(2, "gallery")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "2" == t.activeIndex ? "active" : ""
            }, [t._v("Gallery")])]), t._v(" "), t.type.cases ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(3, "case")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "3" == t.activeIndex ? "active" : ""
            }, [t._v("Cases")])]) : t._e()])]), t._v(" "), c("div", {staticClass: "hidden-xs container mt-div"}, [c("div", {staticClass: "pc-maodian-s font-6"}, [0 != t.type.feature ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(0, "features")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "0" == t.activeIndex ? "activepc" : ""
            }, [t._v("Features")])]) : t._e(), t._v(" "), 0 != t.type.feature ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(1, "specs")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "1" == t.activeIndex ? "activepc" : ""
            }, [t._v("Specs & Compare")])]) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(2, "gallery")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "2" == t.activeIndex ? "activepc" : ""
            }, [t._v("Gallery")])]), t._v(" "), "sku" == t.type.type || t.type.cases && "spu" == t.type.type ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), t.type.cases ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(3, "case")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "3" == t.activeIndex ? "activepc" : ""
            }, [t._v("Cases")])]) : t._e(), t._v(" "), "spu" == t.type.type ? c("a", {
                staticClass: "request-a",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick("", "request")
                    }
                }
            }, [c("div", {staticClass: "detail-rquest font-6 request-hover-btn"}, [t._v("Request a Quote")])]) : t._e()])])]), t._v(" "), c("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.barFixed,
                    expression: "barFixed"
                }], staticClass: "detail-maodian barFixed"
            }, [c("div", {staticClass: "visible-xs"}, [c("div", {staticClass: "detail-maodian-value"}, [0 != t.type.feature ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(0, "features")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "0" == t.activeIndex ? "active" : ""
            }, [t._v("Features")])]) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(1, "specs")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "1" == t.activeIndex ? "active" : ""
            }, [t._v("Specs & Compare")])]) : t._e(), t._v(" "), c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(2, "gallery")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "2" == t.activeIndex ? "active" : ""
            }, [t._v("Gallery")])]), t._v(" "), t.type.cases ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(3, "case")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "3" == t.activeIndex ? "active" : ""
            }, [t._v("Cases")])]) : t._e()])]), t._v(" "), c("div", {staticClass: "hidden-xs container mt-div"}, [c("div", {staticClass: "pc-maodian-s floatLeft font-6"}, [0 != t.type.feature ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(0, "features")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "0" == t.activeIndex ? "activepc" : ""
            }, [t._v("Features")])]) : t._e(), t._v(" "), 0 != t.type.feature ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(1, "specs")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "1" == t.activeIndex ? "activepc" : ""
            }, [t._v("Specs & Compare")])]) : t._e(), t._v(" "), "sku" == t.type.type || 0 != t.type.spec && "spu" == t.type.type ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(2, "gallery")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "2" == t.activeIndex ? "activepc" : ""
            }, [t._v("Gallery")])]), t._v(" "), "sku" == t.type.type || t.type.cases && "spu" == t.type.type ? c("div", {staticClass: "pc-maodian-s-line"}) : t._e(), t._v(" "), t.type.cases ? c("a", {
                staticClass: "clickA",
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick(3, "case")
                    }
                }
            }, [c("div", {
                staticClass: "pc-maodiain-item",
                class: "3" == t.activeIndex ? "activepc" : ""
            }, [t._v("Cases")])]) : t._e()]), t._v(" "), c("div", {staticClass: "detail-maodian-btns font-7"}, [c("a", {
                attrs: {href: "javascript:;"},
                on: {
                    click: function (e) {
                        return e.preventDefault(), t.navchick("", "request")
                    }
                }
            }, [c("div", {staticClass: "request-hover-btn"}, [t._v("Request a Quote")])]), t._v(" "), "sku" == t.type.type ? c("div", {
                staticClass: "request-hover-btn",
                on: {click: t.brochure}
            }, [t._v("Brochure")]) : t._e()])])])])
        }), [], !1, null, null, null);
        e.default = component.exports
    }, 572: function (t, e, c) {
        "use strict";
        var l = c(564);
        c.n(l).a
    }, 575: function (t, e, c) {
        "use strict";
        var l = c(565);
        c.n(l).a
    }, 580: function (t, e, c) {
    }, 581: function (t, e, c) {
    }, 582: function (t, e, c) {
    }, 583: function (t, e, c) {
    }, 584: function (t, e, c) {
    }, 585: function (t, e, c) {
    }, 602: function (t, e, c) {
        "use strict";
        c(86);
        var l = c(15), r = c(563), n = {
            notNextTick: !0,
            loop: !1,
            effect: "slide",
            autoplay: !1,
            speed: 800,
            direction: "horizontal",
            grabCursor: !0,
            on: {
                slideChangeTransitionEnd: function () {
                }
            },
            lazy: {loadPrevNext: !0},
            navigation: {nextEl: ".case-button-next", prevEl: ".case-button-prev"},
            pagination: {el: ".case-swiper-pagination", clickable: !0}
        }, o = {
            props: {value: {}}, components: {myImage: r.a}, data: function () {
                return {swiperConfigCase: n, caselist: [], fit: "fill", previewFlag: !1, imgIndex: 0, top: 0}
            }, created: function () {
                this.caselist = this.value;
                for (var i in this.caselist) this.caselist[i].titlepic = this.caselist[i].titlepic + "?x-oss-process=image/auto-orient,1/resize,m_fill,w_340,h_340/quality,q_90", this.caselist[i].content = this.caselist[i].content.replace(/\"\/d\/file\//gi, '"'.concat(this.base.imgUrl2, "/d/file/"))
            }, methods: {
                previewImgFun: function (t) {
                    Object(l.h)(this), this.previewFlag = !0, this.caseSwiper.slideTo(t)
                }, closePreview: function () {
                    Object(l.a)(this), this.previewFlag = !1
                }
            }
        }, v = (c(632), c(11)), component = Object(v.a)(o, (function () {
            var t = this, e = t.$createElement, c = t._self._c || e;
            return c("div", {
                staticClass: "section-case",
                style: {position: "relative", "z-index": t.previewFlag ? 999999 : 2},
                attrs: {id: "case"}
            }, [null != t.caselist && t.caselist.length > 0 ? c("div", {
                ref: "cases",
                staticClass: "moudle container"
            }, [c("h2", {staticClass: "moudle-title font-2"}, [t._v("Cases")]), t._v(" "), c("div", {staticClass: "moudle-value case-moudle-value"}, [c("div", {staticClass: "case-outer-div"}, t._l(t.caselist, (function (e, l) {
                return c("div", {
                    key: l, staticClass: "service-content-one", on: {
                        click: function (e) {
                            return t.previewImgFun(l)
                        }
                    }
                }, [c("div", {staticClass: "case-image-warp"}, [c("my-image", {
                    attrs: {
                        src: e.titlepic,
                        ratio: 1
                    }
                })], 1), t._v(" "), c("div", {staticClass: "sco-bottom-pc hidden-xs font-8"}, [c("span", [t._v(t._s(e.title))])]), t._v(" "), c("div", {staticClass: "sco-bottom visible-xs font-8"}, [c("span", [t._v(t._s(e.title))])])])
            })), 0)])]) : t._e(), t._v(" "), c("div", {
                directives: [{
                    name: "show",
                    rawName: "v-show",
                    value: t.previewFlag,
                    expression: "previewFlag"
                }], staticClass: "outer-tc case-poup full-screen"
            }, [c("div", {staticClass: "preview-pic"}, [c("div", {
                directives: [{
                    name: "swiper",
                    rawName: "v-swiper:caseSwiper",
                    value: t.swiperConfigCase,
                    expression: "swiperConfigCase",
                    arg: "caseSwiper"
                }]
            }, [c("div", {
                staticClass: "swiper-wrapper",
                staticStyle: {height: "100vh", width: "100vw"}
            }, t._l(t.caselist, (function (e, l) {
                return c("div", {
                    key: "case" + l,
                    staticClass: "swiper-slide"
                }, [c("div", {staticClass: "case-view"}, [c("img", {
                    staticClass: "image",
                    attrs: {src: e.titlepic.split("?")[0]}
                }), t._v(" "), c("div", {staticClass: "case-view-title"}, [t._v(t._s(e.title))]), t._v(" "), c("div", {
                    staticClass: "case-view-desc",
                    domProps: {innerHTML: t._s(e.content)}
                })])])
            })), 0)]), t._v(" "), t._m(0), t._v(" "), t._m(1), t._v(" "), c("div", {staticClass: "case-swiper-pagination swiper-pagination swiper-pagination-bullets"})]), t._v(" "), c("i", {
                staticClass: "el-icon-close preview-close",
                on: {click: t.closePreview}
            })])])
        }), [function () {
            var t = this.$createElement, e = this._self._c || t;
            return e("button", {
                staticClass: "el-carousel__arrow el-carousel__arrow--right case-button-next",
                attrs: {type: "button"}
            }, [e("i", {staticClass: "el-icon-arrow-right"})])
        }, function () {
            var t = this.$createElement, e = this._self._c || t;
            return e("button", {
                staticClass: "el-carousel__arrow el-carousel__arrow--left case-button-prev",
                attrs: {type: "button"}
            }, [e("i", {staticClass: "el-icon-arrow-left"})])
        }], !1, null, null, null);
        e.a = component.exports
    }, 631: function (t, e, c) {
        "use strict";
        var l = c(580);
        c.n(l).a
    }, 632: function (t, e, c) {
        "use strict";
        var l = c(581);
        c.n(l).a
    }, 633: function (t, e, c) {
        t.exports = c.p + "img/360VR.f8511dd.svg"
    }, 634: function (t, e, c) {
        "use strict";
        var l = c(582);
        c.n(l).a
    }, 635: function (t, e, c) {
        "use strict";
        var l = c(583);
        c.n(l).a
    }, 636: function (t, e, c) {
        "use strict";
        var l = c(584);
        c.n(l).a
    }, 637: function (t, e, c) {
        "use strict";
        var l = c(585);
        c.n(l).a
    }
}]);