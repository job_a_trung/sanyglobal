$(function () {

	// 底部轮播图
	$(".section-develop_area .slide-tab .li").click(function () {
		var index = Number($(this)[0].id) - 1;
		$(this).siblings('div').removeClass('active');
		$(this).addClass('active');
		$('.section-develop_area .slide_list li').removeClass('active');
		$('.section-develop_area .slide_list li').eq(index).addClass('active');
	});
	var arrLength = $('.section-develop_area .slide_list li').length;
	var timer = null; //定义空对象存放定时器
	var num = 0;
	function autoPlay() {
		timer = setInterval(function () {
			if (num < (arrLength - 1)) {
				num++; // 切换下一张
			} else if (num === (arrLength - 1)) {
				num = 0;
			}
			$('.section-develop_area .slide_list li').removeClass('active');
			$(".section-develop_area .slide-tab .li").removeClass('active');
			$('.section-develop_area .slide_list li').eq(num).addClass('active');
			$(".section-develop_area .slide-tab .li").eq(num).addClass('active');
		}, 5000)
	}
	setTimeout(autoPlay, 1000); //延时播放，这样就不用一刷新页面立马切换图片


	var screen_width = document.body.clientWidth;
	if (screen_width < 769) {
		$('.section-quick .main-quick .sec_inner ul li a .ico_img img').removeClass('on');
		$('.section-quick .sec_inner .ico_img .off').hide();
		$('.section-quick .sec_inner .ico_img .on').show();
		$('.section-topBanner .btn_playStop').hide();
	}

	//视频轮播
	var videos = [{
		url:'/cms/videos/banner_00.mp4',
		text:'Happy Holidays',
		duration:5,
		poster:'/cms/index/0.jpg',
		time:'2021/1/3 0:0:0'
	},{
		url:'/cms/videos/banner_01.mp4',
		text:'Reach the unreachable',
		duration:4,
		poster:'/cms/index/1.jpg',
		time:'2031/1/3 0:0:0'
	},{
		url:'/cms/videos/banner_02.mp4',
		text:'Built for a better world',
		duration:4,
		poster:'/cms/index/2.jpg',
		time:'2031/1/3 0:0:0'
	},{
		url:'/cms/videos/banner_03.mp4',
		text:'Forward and for more',
		duration:4.5,
		poster:'/cms/index/3.jpg',
		time:'2031/1/3 0:0:0'
	}]
	var videoLen=0;
	var currentIndex = 0;
	var loop = true;
	var processDOM='',videosDom='';
	for(var i=0;i<videos.length;i++){
		var item=videos[i];
		var t1=(new Date(item.time)).getTime(),t2=(new Date()).getTime();
		if(t1>t2){
			processDOM+='<a href="javascript:;"><div class="process1"></div><div class="process2" style="transition-duration:'+item.duration+'s"></div></a>';
			if(videoLen===0){
				$(".section-topBanner .sec_inner").append('<div class="gallery_item on">'+
					'<div class="bg_box">'+
						'<div class="box_img">'+
							'<div class="video_area">'+
								'<video autoplay="true" preload="auto" muted x5-video-player-type="h5" x-webkit-airplay="true" airplay="allow" webkit-playsinline="true" playsinline="true" src="'+img_base_url+item.url+'"'+
								'poster="'+img_base_url+item.poster+'" ></video>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<div class="container"><div class="title">'+item.text+'<br /></div></div>'+
      			'</div>');
			}else{
				$(".section-topBanner .sec_inner").append('<div class="gallery_item">'+
					'<div class="bg_box">'+
						'<div class="box_img">'+
							'<div class="video_area">'+
								'<video preload="auto" muted x5-video-player-type="h5" x-webkit-airplay="true" airplay="allow" webkit-playsinline="true" playsinline="true" src="'+img_base_url+item.url+'"'+
								'poster="'+img_base_url+item.poster+'" ></video>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<div class="container"><div class="title">'+item.text+'<br /></div></div>'+
      			'</div>');
			}
			videoLen++;
			if(i===videos.length-1){
				$(".section-topBanner .carousel_inner").html(processDOM);
				$(".section-topBanner").show();
				var swiperVideos = $(".gallery_item video");
				var swiperItems = $(".section-topBanner .gallery_item");
				var processBars = $(".section-topBanner .carousel_inner a");
				processBars.eq(0).addClass("on");
				swiperVideos.eq(0)[0].play();
				document.addEventListener("WeixinJSBridgeReady", function () {
					swiperVideos.eq(0)[0].play();
				}, false);
				$(".btn_playStop").on("click", function () {
					var btn1 = $(this).find(".btn1");
					loop = !loop;
					if (loop) {
						btn1.show().siblings().hide();
					} else {
						btn1.hide().siblings().show();
					}
				});
				swiperVideos.on("ended", function () {
					currentIndex = swiperVideos.index($(this));
					var nextIndex = loop ? (currentIndex + 1) % videoLen : currentIndex;
					this.pause();
					swiperVideos.eq(nextIndex)[0].play();
					swiperItems.eq(nextIndex).addClass('on').siblings().removeClass('on');
					processBars.eq(nextIndex).addClass('on').siblings().removeClass('on');
				});
				processBars.on("click", function () {
					currentIndex=swiperItems.index($(".section-topBanner .gallery_item.on"));
					var currentVideo = swiperVideos.eq(currentIndex)[0];
					var newIndex = processBars.index($(this));
					currentVideo.currentTime = 0;
					currentVideo.pause();
					currentIndex = newIndex;
					swiperVideos.eq(currentIndex)[0].play();
					swiperItems.eq(currentIndex).addClass('on').siblings().removeClass('on');
					processBars.eq(currentIndex).addClass('on').siblings().removeClass('on');
				})
				swiperItems.on("click", function () {
					currentIndex = swiperItems.index($(this));
					swiperVideos.eq(currentIndex)[0].play();
				})
			}
		}
	}
	$(".section-news .article").each(function () {
		$(this).hover(function () {
			$(".section-news .article").removeClass('on');
			$(this).addClass('on');
		});
	});

	function isMob() {
		if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
			return true;
		}
		return false
	}
})